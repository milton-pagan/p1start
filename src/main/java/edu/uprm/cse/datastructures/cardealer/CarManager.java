package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

/**
 * Implements CRUD operations for a REST API.
 *
 * @author milton-pagan1
 */
@Path("/cars")
public class CarManager {
    private final CircularSortedDoublyLinkedList<Car> carList = CarList.getInstance();


    /**
     * Returns all car objects in the list.
     *
     * @return array of cars
     */
    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public Car[] getAllCars() {
        return CarList.toArray();
    }

    /**
     * Returns the car with the given ID.
     *
     * @param id   ID of target car
     * @return target car
     * @throws NotFoundException car with given ID is not found in the list
     */
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Car getCar(@PathParam("id") long id) {

        for(Car c : carList) {
            if(c.getCarId() == id){
                return c;
            }
        }

        throw new NotFoundException();
    }

    /**
     * Adds given car to the list. Doesn't add car and returns Conflict if ID already exists.
     *
     * @param car  car object to be added
     * @return Created if successful, Conflict if car with given ID already exists
     */
    @POST
    @Path("/add")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addCar(Car car) {

        // Check if object with given ID already exists.
        for(Car c : carList) {
            if(c.getCarId() == car.getCarId())
                return Response.status(409).build();
        }

        carList.add(car);

        return Response.status(201).build();
    }

    /**
     * Updates car with a given ID.
     *
     * @param car   updated car
     * @return OK if successful, NotFound if car with given ID doesn't exists
     */
    @PUT
    @Path("{id}/update")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateCar(Car car) {

        for(Car c : carList) {
            if(c.getCarId() == car.getCarId()){
                carList.remove(c);
                carList.add(car);
                return Response.status(200).build();
            }
        }

        return Response.status(404).build();
    }

    /**
     * Removes car with the given ID from the list.
     *
     * @param id  ID of car to be removed
     * @return OK if successful, NotFound if car with given ID doesn't exists
     */
    @DELETE
    @Path("{id}/delete")
    public Response removeCar(@PathParam("id") long id) {

        for(Car c : carList) {
            if(c.getCarId() == id){
                carList.remove(c);
                return Response.status(200).build();
            }
        }

        return Response.status(404).build();

    }

}
