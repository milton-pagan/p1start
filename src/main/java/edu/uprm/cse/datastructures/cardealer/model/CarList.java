package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

/**
 * CarList class that holds a singleton list of cars.
 * Provides a static method to get instance of such list as well as some auxilary methods.
 *
 * @author milton-pagan1
 */
public class CarList {
    private static CircularSortedDoublyLinkedList<Car> carList = new CircularSortedDoublyLinkedList<>(new CarComparator());;

    public static CircularSortedDoublyLinkedList<Car> getInstance() {
        return carList;
    }

    public static void resetCars() {
        carList = new CircularSortedDoublyLinkedList<>(new CarComparator());
    }

    public static Car[] toArray() {
        Car[] arr = new Car[carList.size()];

        int i = 0;

        for(Car c : carList) {
            arr[i++] = c;
        }

        return arr;
    }
}
