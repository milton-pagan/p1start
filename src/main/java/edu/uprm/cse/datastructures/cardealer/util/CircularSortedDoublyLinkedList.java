package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Implementation of a circular doubly linked list that keeps elements sorted given a comparator.
 *
 * @author milton-pagan1
 */
public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{

    private Node<E> header;
    private int currentSize;
    private Comparator<E> comp;

    public CircularSortedDoublyLinkedList(Comparator<E> comp) {
        this.comp = comp;
        this.currentSize = 0;

        this.header =  new Node<E>();
        this.header.setNext(this.header);
        this.header.setPrevious(this.header);
    }

    @Override
    public Iterator<E> iterator() {
        return new CircularSortedDoublyLinkedListIterator<E>();
    }

    /**
     * Adds an element in its correspondent position as to maintain a sorted list, given the list's comparrator.
     *
     * @param  e element to be added to list
     * @return true if operation is successful
     */
    @Override
    public boolean add(E e) {
        return this.add(this.getSortedPosition(e), e);
    }


    /**
     * Auxilary method to insert an element in a given position.
     *
     * @param index   position where element will me inserted
     * @param e       element to be added
     * @return true given a successful operation
     */
    private boolean add(int index, E e) {
        Node<E> temp = this.getNode(index);
        Node<E> newNode = new Node<E>(e, temp, temp.getPrevious());

        temp.getPrevious().setNext(newNode);
        temp.setPrevious(newNode);

        this.currentSize++;

        return true;
    }


    /**
     * Returns the correct position to insert an element as to mantain a sorted list.
     *
     * @param e  element to be inserted
     * @return index to insert element
     */
    private int getSortedPosition(E e) {

        int index = 0;
        Node<E> temp = this.header.getNext();

        while(temp != header && this.comp.compare(e, temp.getElement()) > 0) {
            temp = temp.getNext();
            index++;
        }

        return index;
    }

    @Override
    public int size() {
        return this.currentSize;
    }

    /**
     * Removes first instance of element e in the list.
     *
     * @param e  element to be removed
     * @return true given a successful removal
     */
    @Override
    public boolean remove(E e) {
        if(e == null) return false;

        int index = this.firstIndex(e);

        return index >= 0 ? this.remove(index) : false;
    }

    /**
     * Removes element at a given position.
     *
     * @param index  position of element to be removed from list
     * @return true given a successful removal
     */
    @Override
    public boolean remove(int index) {
        if(index < 0 || index >= this.currentSize) throw new IndexOutOfBoundsException();

        Node<E> target = this.getNode(index);

        target.getPrevious().setNext(target.getNext());
        target.getNext().setPrevious(target.getPrevious());

        target.clearNode();

        this.currentSize--;

        return true;
    }

    /**
     * Removes all instances of a given element.
     *
     * @param e   element to be removed
     * @return number of elements removed
     */
    @Override
    public int removeAll(E e) {
        int count = 0;
        for(Node<E> temp = this.header.getNext(); temp != header; temp = temp.getNext()) {
            if(temp.getElement().equals(e)) {
                Node<E> prev = temp.getPrevious();

                prev.setNext(temp.getNext());
                temp.getNext().setPrevious(prev);
                temp.clearNode();

                temp = prev;

                count++;
            }
        }

        this.currentSize -= count;

        return count;
    }

    @Override
    public E first() {
        return this.header.getNext().getElement();
    }

    @Override
    public E last() {
        return this.header.getPrevious().getElement();
    }

    /**
     * Returns element at a given position.
     *
     * @param index   position of target element
     * @return element at the given position
     */
    @Override
    public E get(int index) {
        if(index < 0 || index >= this.currentSize) throw new IndexOutOfBoundsException();

        return this.getNode(index).getElement();
    }

    /**
     * Auxilary method that returns the node at the given position.
     *
     * @param index   position of target node
     * @return node at given position
     */
    private Node<E> getNode(int index) {
        int i = 0;
        Node<E> result;

        for(result = this.header.getNext(); i < index; result = result.getNext(), i++);

        return result;
    }

    @Override
    public void clear() {
        while(this.remove(this.first()));
    }

    @Override
    public boolean contains(E e) {
        return this.firstIndex(e) >= 0;
    }

    @Override
    public boolean isEmpty() {
        return this.currentSize == 0;
    }

    /**
     * Return the position of the first instance of a given element.
     *
     * @param e  target element
     * @return postion
     */
    @Override
    public int firstIndex(E e) {
        int i = 0;

        for(Node<E> temp = this.header.getNext(); temp != this.header; temp = temp.getNext(), i++) {
            if(temp.getElement().equals(e)) return i;
        }

        return -1;

    }

    /**
     * Return the position of the last instance of a given element.
     *
     * @param e  target element
     * @return postion
     */
    @Override
    public int lastIndex(E e) {
        int i = this.currentSize - 1;

        for(Node<E> temp = this.header.getPrevious(); temp != this.header; temp = temp.getPrevious(), i--) {
            if(temp.getElement().equals(e)) return i;
        }

        return -1;
    }


    /**
     *  Private class that implements the nodes that compose the list.
     */
    private static class Node<E> {

        private E element;
        private Node<E> next;
        private Node<E> previous;

        public Node(E e, Node<E> next, Node<E> previous) {
            this.element = e;
            this.next = next;
            this.previous = previous;
        }

        public Node() {
            super();
        }

        /**
         * Member method that sets target node's fields to null.
         */
        public void clearNode() {
            this.element = null;
            this.next = this.previous = null;
        }

        //Getters and Setters

        public E getElement() {
            return this.element;
        }

        public void setElement(E element) {
            this.element = element;
        }

        public Node<E> getNext() {
            return this.next;
        }

        public void setNext(Node<E> next) {
            this.next = next;
        }

        public Node<E> getPrevious() {
            return this.previous;
        }

        public void setPrevious(Node<E> previous) {
            this.previous = previous;
        }
    }

    /**
     * Private class that implements the list's iterator.
     */
    private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E> {
        private Node<E> nextNode;

        public CircularSortedDoublyLinkedListIterator() {
            this.nextNode = (Node<E>) header.getNext();
        }

        @Override
        public boolean hasNext() {
            return this.nextNode != header;
        }

        @Override
        public E next() {

            if(this.hasNext()) {
                E next = this.nextNode.getElement();

                this.nextNode = this.nextNode.getNext();

                return next;
            }

            throw new NoSuchElementException();
        }
    }

}
