package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

/**
 * Implements comparator to be used on Car objects.
 *
 * @author milton-pagan1
 */
public class CarComparator implements Comparator<Car> {


    /**
     * Returns the comparison between two cars based on Brand, Model or ModelOption.
     *
     * @param  car1 target car object to be compared
     * @param  car2 car object to be compared with
     * @return integer denoting the result of the comparison
     */
    @Override
    public int compare(Car car1, Car car2) {
        if(!car1.getCarBrand().equals(car2.getCarBrand()))
            return car1.getCarBrand().compareTo(car2.getCarBrand());

        if (!car1.getCarModel().equals(car2.getCarModel()))
            return car1.getCarModel().compareTo(car2.getCarModel());

        return car1.getCarModelOption().compareTo(car2.getCarModelOption());
    }

}
